



###################### Mount Repo for Data Restore #########

###################### Mounting

# Run this command to mount your repository to a empty folder 
# Replace /PATH/TO/YOUR/REPOSITORIE to your repository
# Replace /PATH/TO/restic-repo.password to your restic-repo.password file
# Replace /PATH/TO/EMPTY/DIR to a empty directory
# Run the command and keep the terminal window open

restic -r /PATH/TO/YOUR/REPOSITORIE --password-file /PATH/TO/restic-repo.password  mount /PATH/TO/EMPTY/DIR

